exports.up = (knex) => knex.schema.createTable('state', (table) => {
  table.increments('idstate').unsigned().notNullable();
  table.text('estate');
});

exports.down = (knex) => knex.schema.dropTable('state');
