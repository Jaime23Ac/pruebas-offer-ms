const express = require('express');
const morgan = require('morgan');
const routes = require('./app/routes');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use('/api/offer', routes);


app.listen(app.get('port'), () => {
  console.log(`localhost:${app.get('port')}`);
});


module.exports = app;
