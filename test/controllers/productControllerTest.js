/* eslint-disable no-undef */
const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../index');
const productRepositorie = require('../../app/repositories/productsRepositorie');
const Helper = require('../helper');

const api = 'api/offer/products';
chai.use(chaiHttp);

describe('products crud', () => {
  before(() => Helper.migrate());

  beforeEach(async () => {
    await Helper.clear();
  });

  it('register product  vaidation error', () => chai
    .request(app)
    .post(api)
    .send({})
    .then(assert.fail)
    .catch((error) => {
      assert.equal(error.status, 400);
    }));
  afterEach(() => Helper.clear());
  it('register product validation correct', () => chai
    .request(app)
    .post(api)
    .send({
      nameProduct: 'oipro', characteristics: 'buen sonido', cost: 200, idstate: 2, idcategory: 1,
    })
    .then(async () => {
      const [data] = await productRepositorie.productsbyname('oipro');
      assert.equal(data.nameProduct, 'oipro');
    }));
  afterEach(() => Helper.clear());
  it('get products by id estatus correct', () => chai
    .request(app)
    .get(`${api}/148`)
    .then((res) => {
      const { status } = res;
      assert.equal(status, 200);
    }));
  afterEach(() => Helper.clear());
  it('get products status 200', () => chai
    .request(app)
    .get(api)
    .then((res) => {
      const { status } = res;
      assert.equal(status, 200);
    }));
  afterEach(() => Helper.clear());
  it('get products by id validation error', () => chai
    .request(app)
    .get(`${api}/8000`)
    .catch((error) => {
      assert.equal(error.status, 400);
    }));
  afterEach(() => Helper.clear());
});

describe('products controller validations', () => {
  before(() => Helper.migrate());

  beforeEach(async () => {
    await Helper.clear();
  });

  it('update product status 200', async () => {
    const product = {
      nameProduct: 'genius',
      characteristics: 'comodo',
      cost: 20000,
      idstate: 2,
      idcategory: 4,
    };
    const [products] = await productRepositorie.registerProduct(product);

    return chai
      .request(app)
      .put(`${api}/${products.idProduct}`)
      .send({
        nameProduct: 'genius', characteristics: 'rapido', cost: 20000, idstate: 2, idcategory: 4,
      })
      .then(async (Response) => {
        const { status } = Response;
        assert.equal(status, 200);
      });
  });

  it('update project validation error', async () => {
    const product = {
      nameProduct: 'genius',
      characteristics: 'comodo',
      cost: 20000,
      idstate: 2,
      idcategory: 4,
    };
    const [products] = await productRepositorie.registerProduct(product);

    return chai
      .request(app)
      .put(`${api}/${products.idProduct}`)
      .send({})
      .then(assert.fail)
      .catch((error) => {
        assert.equal(error.status, 400);
      });
  });

  it('delete product validation correct', async () => {
    const product = {
      nameProduct: 'genius',
      characteristics: 'comodo',
      cost: 20000,
      idstate: 2,
      idcategory: 4,
    };

    const [products] = await productRepositorie.registerProduct(product);

    return chai
      .request(app)
      .delete(`${api}/${products.idProduct}`)
      .then((res) => {
        assert.equal(res.status, 200);
      });
  });

  it('delete product validation error', () => chai
    .request(app)
    .delete(`${api}/900`)
    .then(assert.fail)
    .catch((error) => {
      assert.equal(error.status, 400);
    }));

  it('get products by name validation correct', async () => {
    const product = {
      nameProduct: 'genius',
      characteristics: 'comodo',
      cost: 20000,
      idstate: 2,
      idcategory: 4,
    };

    const [products] = await productRepositorie.registerProduct(product);

    return chai
      .request(app)
      .get(`${api}/${products.nameProduct}`)
      .then((res) => {
        const { status } = res;
        assert.equal(status, 200);
      });
  });
});
