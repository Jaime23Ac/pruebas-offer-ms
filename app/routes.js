const { Router } = require('express');

const router = Router();
const productsControllers = require('./controllers/products.controller');

router.post('/products', productsControllers.registerproducts);

router.get('/products', productsControllers.getproducts);

router.get('/products/:id', productsControllers.getproductsByid);

router.delete('/products/:id', productsControllers.deleteproduct);

router.put('/products/:id', productsControllers.updateproduct);

router.get('/productsname/:name', productsControllers.getproductsByname);

// router.get('/productscategory/:idcategory', productsControllers.getproductBycategory);

module.exports = router;
