/* eslint-disable consistent-return */
/* eslint-disable no-undef */
const productsController = module.exports;
const validator = require('../validators/validator');
const schema = require('../validators/controller.schema');
const productservice = require('../services/productsService');

// REGISTER PRODUCT
productsController.registerproducts = async (req, res) => {
  const { body } = req;
  try {
    validator.validate(schema, body);

    return productservice.create(body)
      .then((response) => res.json(response))
      .catch((error) => next(new BaseError(error.message)));
  } catch (error) {
    res.status(400).json({
      status: 'error',
      message: error.message,
    });
  }
};

// GET PRODUCTS
productsController.getproducts = async (req, res) => {
  const getproducts = await productservice.getproducts();
  res.status(200).json(getproducts);
};

// GET PRODUCTS BY ID
productsController.getproductsByid = async (req, res) => {
  const recived = req.params.id;

  try {
    const object = await productservice.getproductById(recived);

    return res.status(200).json(object);
  } catch (error) {
    res.status(400).json({
      status: 'error',
      message: error.message,
    });
  }
};

// DELETE Product
productsController.deleteproduct = async (req, res) => {
  const { id } = req.params;
  try {
    const [sss] = await productservice.getproductById(id);
    validator.validateExist(sss);
    validator.istnumber(id);

    return productservice.deleteproduct(id)
      .then((response) => res.send(response));
  } catch (error) {
    res.status(400).json({
      status: 'error',
      message: error.message,
    });
  }
};

// UPDATE Product
productsController.updateproduct = async (req, res) => {
  const ids = req.params.id;
  const { body } = req;
  try {
    validator.validate(schema, body);
    const [sss] = await productservice.getproductById(ids);
    validator.validateExist(sss);
    validator.istnumber(ids);

    return productservice.updateproduct(ids, body)
      .then((response) => res.send(response))
      .catch((error) => next(new Error(error.message)));
  } catch (error) {
    res.status(400).json({
      status: 'error',
      message: error.message,
    });
  }
};

// GET PRODUCTS BY NAME
productsController.getproductsByname = async (req, res) => {
  // eslint-disable-next-line prefer-destructuring
  const name = req.params;
  try {
    const Data = await productservice.getproductsByname(name);

    return res.status(200).json(Data);
  } catch (error) {
    res.status(400).json({
      status: 'error',
      message: error.message,
    });
  }
};


// GET PRODUCTS BY ID CATEGORY
productsController.getproductBycategory = async (req, res) => {
  const idCategory = req.params.idcategory;

  try {
    const response = await productservice.getproductsbycategory(idCategory);

    return res.status(200).json(response);
  } catch (error) {
    res.status(404).json({
      status: 'error',
      message: error.message,
    });
  }
}
