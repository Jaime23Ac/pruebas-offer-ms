module.exports = {
  title: 'product',
  properties: {
    idProduct: {
      type: 'number',
    },
  },
  required: ['idProduct'],
};
