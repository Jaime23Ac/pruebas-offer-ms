const productsRepositories = module.exports;
const DB = require('../utils/DB');

productsRepositories.registerProduct = (product) => DB('product').insert(product).returning('*');


productsRepositories.getproducts = () => DB('product').select('*').from('product');

productsRepositories.getproductsById = (id) => DB('product').select('*').where({ idProduct: id }).returning('*');

// eslint-disable-next-line max-len
productsRepositories.productsbyname = (name) => DB('product').select('*').where('nameProduct', 'like', `%${name.name}%`).returning('*');

productsRepositories.deleteproduct = (id) => DB('product').delete('*').where({ idProduct: id });

productsRepositories.updateproduct = (id, body) => DB('product').update(body).where({ idProduct: id });

productsRepositories.productsbycategory = (idcategory) => DB('product').select('*').where({idcategory: idcategory}).returning('*');
