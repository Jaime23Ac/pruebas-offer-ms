const productsService = module.exports;
const Service = require('../repositories/productsRepositorie');

const Response = {};

productsService.create = async (products) => {
  Response.status = 400;
  try {
    this.products = products;
    const res = await Service.registerProduct(products);
    Response.status = 200;
    // eslint-disable-next-line prefer-destructuring
    Response.body = res[0];
  } catch (error) {
    // eslint-disable-next-line no-console
    Response.status = 400;
  }
};

productsService.getproducts = async () => {
  const data = await Service.getproducts();

  return data;
};

productsService.getproductById = async (id) => {
  const resp = await Service.getproductsById(id);

  return resp;
};

productsService.deleteproduct = async (id) => {
  await Service.deleteproduct(id);
};

productsService.updateproduct = async (id, body) => {
  await Service.updateproduct(id, body);
};

productsService.getproductsByname = async (name) => {
  const resp = await Service.productsbyname(name);

  return resp;
};

productsService.getproductsbycategory = async (idcategory) => {
  const resp = await Service.getproductsbycategory(idcategory);

  return resp;
};
